﻿using System;

namespace Auction.BLL.DTO
{
    /// <summary>
    /// <see cref="ImagesDTO"/> data transfer class
    /// </summary>
    public class ImagesDTO
    {
        /// <summary>
        /// ImageDTO id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// ImageDTO Image1
        /// </summary>
        public string Image1 { get; set; }
        /// <summary>
        /// ImageDTO Image2
        /// </summary>
        public string Image2 { get; set; }
        /// <summary>
        /// ImageDTO Image3
        /// </summary>
        public string Image3 { get; set; }
        /// <summary>
        /// ImageDTO Image4
        /// </summary>
        public string Image4 { get; set; }
        /// <summary>
        /// ImageDTO Image5
        /// </summary>
        public string Image5 { get; set; }
        /// <summary>
        /// ImageDTO Image6
        /// </summary>
        public string Image6 { get; set; }
        /// <summary>
        /// ImageDTO Image7
        /// </summary>
        public string Image7 { get; set; }
        /// <summary>
        /// ImageDTO Image8
        /// </summary>
        public string Image8 { get; set; }
        /// <summary>
        /// ImageDTO Image9
        /// </summary>
        public string Image9 { get; set; }
    }
}
