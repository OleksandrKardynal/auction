﻿
namespace Auction.BLL.DTO
{
    /// <summary>
    /// <see cref="AskOwnerDTO"/> data transfer class
    /// </summary>
    public class AskOwnerDTO
    {
        /// <summary>
        /// AskOwnerDTO Owner Email
        /// </summary>
        public string OwnerEmail { get; set; }
        /// <summary>
        /// AskOwnerDTO Text 
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// AskOwnerDTO FullName
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// AskOwnerDTO User Email
        /// </summary>
        public string UserEmail { get; set; }
    }
}
