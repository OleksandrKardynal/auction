﻿
namespace Auction.DAL.Entities
{
    /// <summary>
    /// Ask Owner
    /// </summary>
    public class AskOwner
    {
        /// <summary>
        /// Gets and sets Owener Email
        /// </summary>
        public string OwnerEmail { get; set; }
        /// <summary>
        /// Gets and sets uswers text
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// Gets and sets full name user
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// Gets and sets user email
        /// </summary>
        public string UserEmail { get; set; }
    }
}
