﻿
namespace Auction.WepApi.Models
{
    /// <summary>
    /// GoogleAuthViewModel
    /// </summary>
    public class GoogleAuthViewModel
    {
        /// <summary>
        /// GoogleAuthViewModel Provider="Google"
        /// </summary>
        public string Provider { get; set; }
        /// <summary>
        /// GoogleAuthViewModel IdToken
        /// </summary>
        public string IdToken { get; set; }
    }
}
