﻿
namespace Auction.WepApi.Models
{
    /// <summary>
    /// ForgotPasswordViewModel
    /// </summary>
    public class ForgotPasswordViewModel
    {
        /// <summary>
        /// ForgotPasswordViewModel Email
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// ForgotPasswordViewModel ClientURI
        /// </summary>
        public string ClientURI { get; set; }
    }
}
