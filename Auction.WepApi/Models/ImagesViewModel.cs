﻿using System;

namespace Auction.WepApi.Models
{
    /// <summary>
    /// ImagesViewModel
    /// </summary>
    public class ImagesViewModel
    {
        /// <summary>
        /// ImagesViewModel Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// ImagesViewModel Image1
        /// </summary>
        public string Image1 { get; set; }
        /// <summary>
        /// ImagesViewModel Image2
        /// </summary>
        public string Image2 { get; set; }
        /// <summary>
        /// ImagesViewModel Image3
        /// </summary>
        public string Image3 { get; set; }
        /// <summary>
        /// ImagesViewModel Image4
        /// </summary>
        public string Image4 { get; set; }
        /// <summary>
        /// ImagesViewModel Image5
        /// </summary>
        public string Image5 { get; set; }
        /// <summary>
        /// ImagesViewModel Image6
        /// </summary>
        public string Image6 { get; set; }
        /// <summary>
        /// ImagesViewModel Image7
        /// </summary>
        public string Image7 { get; set; }
        /// <summary>
        /// ImagesViewModel Image8
        /// </summary>
        public string Image8 { get; set; }
        /// <summary>
        /// ImagesViewModel Image9
        /// </summary>
        public string Image9 { get; set; }
        /// <summary>
        /// ImagesViewModel Image lot Id
        /// </summary>
        //public int LotId { get; set; }
    }
}
