﻿
namespace Auction.WepApi.Models
{
    /// <summary>
    /// AskOwnerViewModel
    /// </summary>
    public class AskOwnerViewModel
    {
        /// <summary>
        /// AskOwnerViewModel ownerEmail
        /// </summary>
        public string OwnerEmail { get; set; }
        /// <summary>
        /// AskOwnerViewModel Text message
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// AskOwnerViewModel FullName
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// AskOwnerViewModel UserEmail
        /// </summary>
        public string UserEmail { get; set; }
    }
}
