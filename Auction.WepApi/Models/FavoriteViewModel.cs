﻿
using System;

namespace Auction.WepApi.Models
{
    /// <summary>
    /// FavoriteViewModel
    /// </summary>
    public class FavoriteViewModel
    {
        /// <summary>
        /// FavoriteViewModel Id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// FavoriteViewModel UserId
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// FavoriteViewModel LotId
        /// </summary>
        public int LotId { get; set; }
    }
}
