import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { AdminService } from '../../services/admin.service';
import { tap } from "rxjs/operators";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  users: User[];

  constructor(private adminService: AdminService,
    private toastrService: ToastrService) { }

  public getUsers() {
    this.adminService.getUsersWithRoleUser()
      .pipe(
        tap(users => this.users = users)
      )
      .subscribe();
  }

  public ngOnInit(): void {
    this.getUsers();
  }

  public deleteUser(userId: string): void {
    this.adminService.deleteUser(userId)
      .subscribe(response => {
        this.toastrService.success("User is deleted!");
        this.getUsers();
      }, _ => {
        this.toastrService.error("Error!");
      });
  }
}
