export class Favorite {
    id: string;
    userId: string;
    lotId: number;
}