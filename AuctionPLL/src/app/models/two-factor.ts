export class TwoFactor {
    email: string;
    provider: string;
    token: string;
}