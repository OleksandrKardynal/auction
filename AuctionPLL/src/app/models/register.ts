export class Register {
    Name: string;
    Surname: string;
    Role: string;
    Email: string;
    Password: string;
    ClientURI: string;
}