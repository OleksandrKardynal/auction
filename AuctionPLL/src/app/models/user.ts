export class User {
    Id: string;
    Name: string;
    Surname: string;
    Role: string;
    Email: string;
    Password: string;
}