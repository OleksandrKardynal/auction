export class LotState {
    Id: number;
    OwnerId: string;
    FutureOwnerId: string;
    CountBid: number;
    LotId: number;
}