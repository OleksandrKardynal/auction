import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  localStorage: Storage;

  constructor() {
    this.localStorage = window.localStorage;
  }

  public get(key: string) {
    return this.localStorage.getItem(key);
  }

  public set(key: string, value: any) {
    this.localStorage.setItem(key, value);
  }

  public remove(key: string) {
    this.localStorage.removeItem(key);
  }

  public clear() {
    this.localStorage.clear();
  }
}