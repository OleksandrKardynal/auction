import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Comment } from '../models/comment';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class CommentService {
    apiUrl = 'https://localhost:44325/api/comment/';
    constructor(private httpClient: HttpClient) { }

    public addComment(comment: Comment) {
        return this.httpClient.post(this.apiUrl, comment);
    }

    public getCommentsByLotId(lotId: number): Observable<Comment[]> {
        return this.httpClient.get<Comment[]>(this.apiUrl + lotId);
    }

    public deleteVommentById(commentId: string) {
        return this.httpClient.delete(this.apiUrl + commentId);
    }
}