import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AdminService {
    apiUrl = 'https://localhost:44325/api/admin/';
    constructor(private httpClient: HttpClient) { }

    public getUsersWithRoleUser(): Observable<User[]> {
        return this.httpClient.get<User[]>(this.apiUrl + "users/");
    }

    public deleteUser(userId: string) {
        return this.httpClient.delete(this.apiUrl + userId);
    }
}