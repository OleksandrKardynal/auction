import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Favorite } from '../models/favorite';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class FavoriteService {
    apiUrl = 'https://localhost:44325/api/favorite/';
    constructor(private httpClient: HttpClient) { }

    public addFavorite(favorite: Favorite) {
        return this.httpClient.post(this.apiUrl, favorite);
    }

    public deleteFavoriteById(id: string) {
        return this.httpClient.delete(this.apiUrl + id);
    }

    public getUserFavorite(userId: string): Observable<Favorite[]> {
        return this.httpClient.get<Favorite[]>(this.apiUrl + userId);
    }

    public getFavoriteByUserIdAndLotId(favorite: Favorite): Observable<Favorite> {
        return this.httpClient.post<Favorite>(this.apiUrl + "favorite/", favorite);
    }

    public deleteFavoriteByUserIdAndLotId(favorite: Favorite) {
        return this.httpClient.post(this.apiUrl + "deletepost", favorite);
    }
}